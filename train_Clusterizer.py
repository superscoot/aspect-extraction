"""
Natrenuje model Clusterizeru a ulozi ho do suboru. Pre zaradenie noveho dokumentu do clustru tak nebude potrebene spustat opakovane ucenie

Created on Mon Nov  5 10:31:37 2018
@author: roman
"""

from time import time
from classes.systemFactory import SystemFactory
import settings
import pickle
from database.postgres import Postgres
from database.mongo import Mongo
import json
import matplotlib.pyplot as plt
import numpy as np

TIME0 = time()

system = SystemFactory()
system.build_load_system()

# Pripojenie k databazi
mongo = Mongo()
mongo.connect(database='mta_products', collection='refer')


## Nacitaj dataset z MTA DB
dataset = []
for product_category in ['televisions']:
    for review in mongo.get_all_product_reviews(product_category=product_category):
        for sentiment in ('pros', 'cons'):
            try:
                dataset.extend(review['author_sentiment'][sentiment])
            except KeyError:
                pass
del product_category, sentiment, review
    
    
## Preprocesuj a uloz vstup
dataset = system.preprocessor.clean(dataset=dataset)
system.preprocessor.saveOutput('./dataset/tvs_processed.bin')
# Nacitaj dataset zo suboru
with open ('./dataset/tvs_processed.bin', 'rb') as f:
    dataset = pickle.load(f)
f.close()



## Vyber 10% najfrekventovanejsich viet
cnt = {}
for idx, item in enumerate(dataset['cleaned']):
    key = '_'.join(item)
    if key not in cnt:
        cnt.update({key: {'count': 1, 'raw': dataset['raw'][idx]}})
    else:
        cnt[key]['count'] += 1

sortedFreq = sorted(list(cnt.items()), key=lambda x: x[1]['count'], reverse=True)
topItems = {'cleaned': [], 'raw': []}
for item in sortedFreq:
    if item[1]['count'] >= 2:
        topItems['cleaned'].append(item[0].split('_'))
        topItems['raw'].append(item[1]['raw'])
del item, key, idx


# Natrenuj clusterizer a uloz do suboru
N_CLUSTERS = 11
DIST_METRIC = 'mean'
a = system.clusterizer.clusterize(dataset=topItems , n_clusters=N_CLUSTERS, dist_metric=DIST_METRIC)


#system.clusterizer.get_WCSS(dataset, lower_boundary=5, upper_boundary=20)

#print(system.clusterizer.model.inertia_)
#print(max(system.clusterizer.get_clusters_std()))
system.clusterizer.save_to_file(path='./clusters/tvs/top_items_clusters_n' + str(N_CLUSTERS) + '_metric' + str(DIST_METRIC).capitalize() + '.json',
               item_name='All', 
               item_category='Positive and negative merged')

rules = {
    'ignore': ['2', '22'],
    'secondRun': ['4'],
    'concat': {
        '25': '1',
        '5': '3',
        '9': '23',
        '31': '11',
        '28': '10',
        '13': '24',
        '6': '24'                
    }        
}
system.clusterizer.set_rules(rules)


#system.clusterizer.saveModel(settings.PATH_TO_CLUSTER_MODEL)
print('hotovo za %s' % str(time()-TIME0))