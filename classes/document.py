"""
Created on Wed Jul  4 14:25:57 2018

@author: roman
"""


class Document:
    
    def __init__(self, text, raw_text=None, centroid_distance=None, cluster_label=None, vector=None, sentiment=None, published_at=None, retrieved_at=None):
        self.text = text
        self.raw_text = raw_text
        self.centroid_distance = centroid_distance
        self.cluster_label = cluster_label
        self.vector = vector
        self.sentiment = sentiment
        self.published_at = published_at
        self.retrieved_at = retrieved_at
        
        
    def get_cluster_label(self):
        return self.cluster_label
        
    def get_text(self):
        return self.text
    
    def get_raw_text(self):
        return self.raw_text
    
    def get_centroid_dist(self):
        return self.centroid_distance
    
    def get_vector(self):
        return self.vector