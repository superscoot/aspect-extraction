# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 15:57:49 2018

@author: roman

Vyznam morfologicky znacek 
N 	substantivum (podstatné jméno)
A 	adjektivum (přídavné jméno)
P 	pronomen (zájmeno)
C 	numerál (číslovka, nebo číselný výraz s číslicemi)
V 	verbum (sloveso)
D 	adverbium (příslovce)
R 	prepozice (předložka)
J 	konjunkce (spojka)
T 	partikule (částice)
I 	interjekce (citoslovce)
X 	neznámý, neurčený, neurčitelný slovní druh
Z 	interpunkce, hranice věty 

"""

from .model import Model 
import pickle
import dill
from nltk import RegexpTokenizer
import re
import os.path
import time
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.pipeline import Pipeline
import time
import datetime




class POS_Tagger(Model):

    def __init__(self):
        super().__init__()
        self.model = Pipeline([
            ('vectorizer', DictVectorizer(sparse=True)),
            ('classifier', DecisionTreeClassifier(criterion='entropy'))
        ])

    
    # Nauci model na zaklade zadanej trenovacej viet
    # *param limit = pocet viet zo vstupneho datasetu
    # *param samples = pocet uciacich vzorkov z kazdej mnoziny (tzn. slov a ich morfologickych znaciek)
    # Ak chceme vidiet uspesnost, pridame testovaciu mnozinu parametrom test_sentences
    def trainModel(self, training_set, samples=10000, **kwargs):
        t0 = time.time()
        X, y = self.transform_to_dataset(training_set)
        
        self.model.fit(X[:samples], y[:samples])
        print('Trenovanie %s vzorkov dokoncene za %fs' % (samples, time.time() - t0))
        # Vyhodnotenie treningu
        if 'test_set' in kwargs:
            print('Score:', self.testModel(kwargs['test_set']))    

    
    # Otestuje a vyhodnoti uspesnost ucenia
    def testModel(self, test_set):
        X_test, y_test = self.transform_to_dataset(test_set)
        return self.model.score(X_test, y_test)
        
    
    # Vrati objekt vlastnosti slova vo vete na pozicii danej indexom
    # Vstup: 'Foo bar baz', 1
    # Vystup objekt vlastnosti slova 'bar'
    def features(self, sentence, index):
        digitPattern = re.compile('[^\w]')
        
        return {
            'word': sentence[index].lower(),
            'is_first': index == 0,
            'is_last': index == len(sentence) - 1,
            'is_capitalized': sentence[index][0].upper() == sentence[index][0],
            'prefix-1': sentence[index][0].lower(),
            'prefix-2': sentence[index][:2].lower(),
            'prefix-3': sentence[index][:3].lower(),
            'suffix-1': sentence[index][-1].lower(),
            'suffix-2': sentence[index][-2:].lower(),
            'suffix-3': sentence[index][-3:].lower(),
            'prev_word': '' if index == 0 else sentence[index - 1].lower(),
            'next_word': '' if index == len(sentence) - 1 else sentence[index + 1].lower(),
            'has_hyphen': '-' in sentence[index],
            'is_numeric': digitPattern.sub('', sentence[index]).isdigit(),
            'capitals_inside': sentence[index][1:].lower() != sentence[index][1:]
        }
    
    
    # Odstrani zo zadanych viet
    # Vstup: [('Jako', 'jako', 'J,), ('bonus', 'bonus' 'NN'), ('kazeta, 'kazeta', 'NN')]
    # Vystup: ['Jako', 'bonus', 'kazeta']
    def untag(self, tagged_sentence):
        return [word for word, lemma, morph in tagged_sentence]
    
    
    # Prevedie vstup na mnozinu vlastnosti slov a ich morfologickych znaciek
    # Vstup: [[('Jako', 'jako', 'J,), ('bonus', 'bonus' 'NN'), ('kazeta, 'kazeta', 'NN')]]
    # Vystup:
    #   X: List objektov features pre kazde slovo [{word: jako, 'is_first':True ... }]
    #   y: List morfologickych znaciek ['NN', 'NN']
    def transform_to_dataset(self, tagged_sentences):
        X, y = [], []
        for tagged in tagged_sentences:
            for index in range(len(tagged)):
                X.append(self.features(self.untag(tagged), index))
                y.append(tagged[index][2][:2])
        return X, y
    
    
    # Tokenizuje vetu a na zaklade nauceneho modelu sa pokusi urcit slovny druh pre kazde slovo
    # Vstup: Tokenizovany retazec: ['Jako', 'bonus']
    # Vystup: [('Jako', 'NN'), ('bonus', 'NN')]
    def pos_tag(self, sentence):
        if sentence:
            # Moze sa stat ze tokenizacia vrati prazdny retazec
            tags = self.model.predict([self.features(sentence, index) for index in range(len(sentence))])
            return list(zip(sentence, tags))
        else:
            return None
    
    
    # Ulozi nauceny model pre neskorsie pouzitie bez nutnosti opakovaneho ucenia
    # param overwrite vynucuje prepisanie uz existujuceho modelu
    def saveModel(self, pathToFile, overwrite=False):
        pathToFile = self.chompTimeStamp(pathToFile)
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M')
        if not overwrite and os.path.isfile(pathToFile + timestamp):
            raise FileExistsError('Zadaný súbor už existuje, ak ho chcete prepísať nastavte parameter overwrite=True')
        with open(pathToFile + timestamp, 'wb') as f:
            pickle.dump(self.model, f)
        f.close()
        

    # Nacita a inicializuje nauceny model. Nie je potrebne znova ucit
    def loadModel(self, pathToFile):
        try:
            with open (pathToFile, 'rb') as f:
                self.model = dill.load(f)
            f.close()
        except FileNotFoundError:
            if self.downloadModel(pathToFile):
                self.loadModel(pathToFile)
            else:
                raise SystemExit('%s cannot be loaded!' % self.chompTimeStamp(pathToFile))