"""
Trieda riesi clustrovanie dokumentov

Created on Wed Jun  6 09:28:38 2018

@author: roman
"""

from .cluster import Cluster
from .document import Document
from .model import Model
from .feature import Feature
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn import metrics
import numpy as np
import io
import json
from scipy.spatial import distance
import dill
import math
import logging
import time
import datetime
from sklearn.preprocessing import StandardScaler
from pathlib import Path
from sklearn.metrics import davies_bouldin_score

class Clusterizer(Model):
    
    
    def __init__(self, min_df=1, use_idf=True, vsm=None):
        super().__init__(vsm=vsm)
        self.centroids = None
        self.datasetMatrix = None
        self.documents = []
        self.clusters = {}
        self.dist_metric = None
        self.rules = {
                'ignore': [], 
                'concat': {}
                }
        
       
    # Setter rules
    def set_rules(self, rules):
        self.rules = rules
        
    
    # Rozdeli dataset do N clusters.
    # Parameter dist_metric urcuje metriku kalkulacie max povolenej vzdialenosti dokumentu od centroidu.
    # Dist_metric=None - ziadna metrika, akceptujeme vsetky dokumenty
    def clusterize(self, dataset, n_clusters, dist_metric):
        logging.info('Clustrovanie dokumentov zacalo')
        self.documents = []
        self.dist_metric = dist_metric
        self.model = KMeans(n_clusters=n_clusters, 
                            init='k-means++', 
                            random_state=0, 
                            max_iter=10000, 
                            n_init=50, 
                            verbose=False)
        
        self.datasetMatrix = self.vsm.transform(dataset['cleaned'])
#        self.datasetMatrix = StandardScaler().fit_transform(self.datasetMatrix)
        self.model.fit_predict(self.datasetMatrix)
        self.initDocuments(dataset=dataset, dist_metric=dist_metric)
        self.initClusters()
        logging.info('Clustrovanie dokumentov dokoncene.\n Inertia: %s,\n max. rozptyl: %s' 
                     % ( str(self.model.inertia_), str(max(self.get_clusters_std())) ))
        return self.clusters
  
    
    # Vrati list rozptylov pre jednotlive clustre
    def get_clusters_std(self):
        return [cluster.get_st_deviation() for cluster in self.clusters.values()]
            
    
    # Osekne realne cislo na n desatinnych miest bez zaokruhlenia
    def truncate_real(f, n):
        return math.floor(f * 10 ** n) / 10 ** n
    
    
    # Ulozi nauceny model pre neskorsie pouzitie bez nutnosti opakovaneho ucenia
    # param overwrite vynucuje prepisanie uz existujuceho modelu
    def saveModel(self, pathToFile):
        pathToFile = self.chompTimeStamp(pathToFile)
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M')
        f = open(pathToFile + timestamp, 'wb')       
        obj = {
            'model': self.model,
            'datasetMatrix': self.datasetMatrix,
            'clusters': self.clusters,
            'dist_metric': self.dist_metric,
            'rules': self.rules
        }
        dill.dump(obj, f)
        f.close()
        
            
    # Inicializuje objekt typu dokument. 
    # Ku kazdemu dokumentu priradi label (prislusnost ku clustru) a kosinovu vzdialenost od centroidu
    # Ak je zadany parameter dist_metric:
    #   mean: dokument je zaradeny do kolekcie ak jeho vzdialenost od centra nepresiahne priemernu hodnotu vzdialenosti vsetkych dokumentov v clustri
    #   None: ziadna metrika, dokument sa do kolekcie dostane vzdy
    def initDocuments(self, dataset, dist_metric):
        self.documents = []
        # Dokument zardujeme na zaklade metriky mean
        if dist_metric and dist_metric == 'mean':
            # Zisti vzdialenosti vsetkych dokumentu v ramci clustru od jeho centroidu
            centroidDistances = {label: np.array([]) for label in set(self.model.labels_)}
            for idx, label in enumerate(self.model.labels_):
                centroidDistances[label] = np.append(centroidDistances[label], np.array(distance.cosine(self.datasetMatrix[idx], self.model.cluster_centers_[label])) )
            # Vypocitaj priemer vzdialenosti dokumentov od centroidu
            for label in centroidDistances:
                centroidDistances[label] = np.mean(centroidDistances[label])
        # Ziadna metrika sa neposudzuje (pre kazdy cluster plati maximalny mozny priemer vzdialenosti, tzn. 1)
        else:
            centroidDistances = {label: np.array([1]) for label in set(self.model.labels_)}
        # Vyber dokumenty, ktore nepresahuju vypocitanu vzdialenost
        for idx, label in enumerate(self.model.labels_):
            centroid_distance = distance.cosine(self.datasetMatrix[idx], self.model.cluster_centers_[label])
            if centroid_distance <= centroidDistances[label]:
                doc = Document(
                        text=' '.join(dataset['cleaned'][idx]),
                        raw_text=''.join(dataset['raw'][idx]),
                        centroid_distance=centroid_distance,
                        cluster_label=label)
                self.documents.append(doc)

    
    # Inicializuje a vrati slovnik objektov triedy Cluster
    # Kazdy cluster ma atributy:
    #   - label = identifikator clustru
    #   - centroid = suradnice stredu (priemeru) clustru
    #   - documents = list objektov triedy Document spadajuci do daneho clustru
    def initClusters(self):
        clusters = {}
        for label in set(self.model.labels_):
            c = Cluster(label=label, 
                        centroid=self.model.cluster_centers_[label],
                        documents=self.get_documents_by_label(label))
            clusters.update({str(label): c})
        self.clusters = clusters
        return self.clusters
       
    
    # Vrati vsetky dokumenty zadaneho clustru
    def get_documents_by_label(self, label):
        return [doc for doc in self.documents if doc.get_cluster_label() == label]
    
    
    # Vrati dictionary alebo jeden cluster ak je zadany parameter label
    def get_clusters(self, label=None):
        if label is not None:
            return self.clusters[str(label)]
        else:
            return self.clusters
    
    
    # Zadanemu dokumentu priradi cluster, do ktoreho by mal spadat
    # Metoda prevedie dokument na vektor suradnic vo vsm
    # Na vstupe sa ocakava predspracovany, ocisteny text
    # Vystupom je label clustru a vypocitany vektor dokumentu
    def predict_label_by_document(self, document):
        docVector = self.vsm.transform([document])
        label = str(self.model.predict(docVector)[0])
        # Label musi prejst sadou definovanych pravidiel
        if label in self.rules['ignore']:
            return None, docVector
        elif label in self.rules['concat']:
            label = self.rules['concat'][label]
        elif label in self.rules['secondRun']:
            # Ziskaj najcetnejsi ngram clustru a ten odstran z textu
            topNgram = self.clusters[label].get_ngram_freq()[0][0]
            newDoc = ' '.join(document).replace(topNgram, '').split()
            if newDoc and (newDoc != document):
                # Ak doslo ku zmene skus znovu klasifikovat, inak zahod
                return self.predict_label_by_document(newDoc)
            else:
                return None, docVector
        return label, docVector
       
          
    # Pre zadany ocisteny text vytvory objekt triedy Document
    def get_document_object(self, document, raw_text=None, sentiment=None):
        label, docVector = self.predict_label_by_document(document=document)
        # Vypocitaj vzdialenost dokumentu o centra clustru
        centroid_distance = distance.cosine(docVector, self.model.cluster_centers_[label])
        doc = Document(text=document, raw_text=raw_text, centroid_distance=centroid_distance, cluster_label=label, vector=docVector)
        return doc        
          

    # Zo zadaneho datasetu zaradi kazdy dokument do prave jedneho clustru
    # V ramci clustru vypocita centroid dokumentov
    def get_feature_by_label(self, input_data, products_id, sentiment=None):
        documents = []
        features = []
        # Pre kazdy dokument vytvor jeho objekt triedy Document
        for idx, doc in enumerate(input_data['cleaned']):
            document_object = self.get_document_object(document=doc, raw_text=input_data['raw'][idx], sentiment=sentiment)
            documents.append(document_object)
        # Zoskup dokumenty podla labelu a vyber "priemerny" dokument 
        for label in set(self.get_documents_labels()):
            label_docs = []
            # Vyber dokumenty spadajuce do rovnakeho clustru
            for idx, doc in enumerate(documents):
                if doc.get_cluster_label() == label:
                    label_docs.append(documents.pop(idx))
            if label_docs:
                # Vytvor objekt Feature
                feature = Feature(
                        products_id = products_id,
                        features_text_id = label)
                features.append(feature)
        return features
               
                     
    # Vypocitaj a vrat vzdialenost dvoch vektorov
    def calculate_vector_distances(self, vector1, vector2):
        return distance.cosine(vector1, vector2)
           
    
    # Vrati list suradnic vsetkych centroidov
    def get_centroids(self):
        return self.model.cluster_centers_
    
    
    # Vrati list labelov pre vsetky dokumenty v naucenom modeli
    def get_documents_labels(self):
        return self.model.labels_
    
    
    # Vrati citatelny vystup clusteringu
    def get_dump(self):
        c_dump = {'clusters': []}
        for item in self.clusters:
            c_dump['clusters'].append(self.clusters[item].dump())
        # zorad clustre podla rozptylov (najmensi rozptyl na zaciatku)
        c_dump['clusters'].sort(key=lambda x: x['stdDeviation'])
        return c_dump
            
    
    # Zapise vystup do suboru  v zadanom formate
    def save_to_file(self, path, f_format=None):
        directory = '/'.join(path.split('/')[:-1])
        file = path.split('/')[-1]
        Path(directory).mkdir(parents=True, exist_ok=True)
        with io.open(directory + '/' + file, 'w', encoding='utf-8') as f:
            if not f_format:
               obj = self.get_dump(), 
               
            if f_format.lower() == 'nn_classifier':
                """
                Vystup v strukture:
                    [{
                        "label": <str>,
                        "docs_count": <int>,
                        "documents": <list>
                    "}, ...]
                """
                obj = [{
                        "label": item['label'], 
                        "cluster_id": "",
                        "docs_count": len(item['documents']),
                        "documents": [doc[0] for doc in item['documents']]
                        } for item in self.get_dump()['clusters']]
            
            f.write(json.dumps(obj, 
                               indent=4, 
                               sort_keys=False, 
                               ensure_ascii=False))
            
    
    
    # Vykresli clustre do 2D grafu
    def plot_clusters(self):
        tsne_init = 'pca'
        tsne_perplexity = 20.0
        tsne_early_exaggeration = 4.0
        tsne_learning_rate = 10
        model = TSNE(n_components=2, random_state=0, init=tsne_init, perplexity=tsne_perplexity,
                 early_exaggeration=tsne_early_exaggeration, learning_rate=tsne_learning_rate)     
        reducedDimensions = model.fit_transform(self.datasetMatrix)
        #reducedDimensions = StandardScaler().fit_transform(reducedDimensions)
        labels = self.model.labels_.reshape(len(self.model.labels_), 1)
        reducedDimensions = np.unique(
                                np.append(reducedDimensions, labels, axis=1), 
                               axis=0)
        plt.scatter(reducedDimensions[:,0], reducedDimensions[:,1], c=reducedDimensions[:,2])
    
#        for i in range (0, self.model.n_clusters):
#            label = ('C ' + str(i))
#            plt.scatter(transformed_clusters[self.model.labels_ == i, 0], transformed_clusters[self.model.labels_ == i, 1], label=label, c=cmap(i))
        plt.legend()
#        plt.show()
#        return transformed_clusters

    def get_davies_bouldin_score(self, dataset, lower_boundary=2, upper_boundary=5, title='Davies Bouldin score'):
        i = lower_boundary
        db_index = []
        x_axis = []
        
        while i <= upper_boundary:
            X = self.vsm.transform(dataset['cleaned'])
            self.clusterize(dataset=dataset, n_clusters=i, dist_metric='mean')
            db_index.append(davies_bouldin_score(X, self.model.labels_))
            x_axis.append(i)
            i += 1
        print(f'Best separation: {db_index.index(min(db_index))} clusters (BDI: {min(db_index)})')
        plt.plot(x_axis, db_index)
        plt.title(title)
        plt.xlabel('Clusters count')
        plt.ylabel('Davies Bouldin score')
        plt.show()
   

    # Odhad najvhodnejsieho poctu clustrov
    def get_WCSS(self, dataset, lower_boundary=2, upper_boundary=5, title='WCSS plot'):
        try:
            wcss = []
            x_axis = []
            i = lower_boundary
            while i <= upper_boundary:
                self.clusterize(dataset=dataset, n_clusters=i, dist_metric='mean')
                wcss.append(self.model.inertia_)
                x_axis.append(i)
                i *= 2
            plt.plot(x_axis, wcss)
            plt.title(title)
            plt.xlabel('Clusters count')
            plt.ylabel('WCSS')
            plt.show()
        except IndexError:
            print('ERROR: Nezadana ziadna mnozina dokumentov, pouzite metodu fit!')
#        
#        
    # Odhad najvhodnejsieho poctu clustrov silhouette metodou
    def get_silhouette(self, dataset, lower_boundary=2, upper_boundary=10):
        try:
            scores = []
            for i in range(lower_boundary, upper_boundary):
                self.clusterize(dataset=dataset, n_clusters=i, dist_metric='mean')
                avg_values = metrics.silhouette_score(self.datasetMatrix, self.model.labels_, metric='cosine')
                values = metrics.silhouette_samples(self.datasetMatrix, self.model.labels_, metric='cosine')
                print(i, '->', avg_values)
                scores.append(avg_values)
                
            plt.plot(range(lower_boundary, upper_boundary), scores)
            plt.title('Silhouette plot')
            plt.xlabel('Clusters count')
            plt.ylabel('Score')
            plt.show()
        except IndexError:
            print('ERROR: Nezadana ziadna mnozina dokumentov, pouzite metodu fit!')
