# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 19:17:33 2019

@author: roman
"""
import os, sys
#sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import dill
import logging
import settings
import requests
import os
import sys
from abc import ABC, abstractmethod
from deprecated import deprecated
import datetime



class Model(ABC):
    """
    Trieda slúži ako všeobecný predok pre implementáciu ďalších modelov a klasifikátorov.
    
    Attributes
    -------
    model : Model, optional
        Všeobecný atribut udržiavajúci objekt modelu.
    tfidfVec : TfidfEmbeddingsVectorizer, optional
        Odkaz na objekt TfidfEmbeddingsVectorizer
        
    Methods
    -------
    saveModel(tfidfVec)
        Nastaví attribut tfidfVec
    
    saveModel(pathToFile)
        Uloží model do súboru podľa zadanej cesty. @abstractmethod
        
    loadModel(pathToFile)
        Načíta a inicializuje naučený model podľa zadanej cesty.
    
    """
    
    def __init__(self, vsm=None):
        self.model = None
        self.vsm = vsm
        self.last_update = None
        
    
    @deprecated(reason="This method is deprecated, we do not use tfidf weights anymore")
    def setTfidfVec(self, tfidfVec):
        """
        Nastaví attribut tfidfVec
        Attributes
        -------
        tfidfVec : TfidfEmbeddingsVectorizer
            Odkaz na objekt TfidfEmbeddingsVectorizer
        """
        self.tfidfVec = tfidfVec

        
    def archiveModel(self, pathToFile):
        """
            Presunie aktualny model do archivu a k nazvu suboru prida casove razitko
        """
        model_name = pathToFile.split('/')[-1]                              # Nazov suboru
        model_path = '/'.join(pathToFile.split('/')[:-1]) + '/archive'      # Umiestnenie suboru
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        if not self.last_update:
            self.last_update = datetime.datetime.now().isoformat()
        newPath = '%s/%s:%s' % (model_path, model_name, self.last_update)
        os.rename(pathToFile, newPath)
        
        
    @abstractmethod
    def saveModel(self, pathToFile):
        pass
        
        
    def loadModel(self, pathToFile):
        """
        Načíta a inicializuje naučený model
        
        Parameters
        -------
        pathToFile : string 
            Relatívna alebo absolútna cesta k súboru
        """
        try:
            with open (pathToFile, 'rb') as f:
                obj = dill.load(f)
            f.close()
            for key, value in obj.items():
                setattr(self, key, value)
            logging.info('loading %s from %s' % (self.__class__.__name__, pathToFile))
        except FileNotFoundError:
            logging.error('File %s not found! Downloading from %s' % (pathToFile, settings.MTA_MODEL_URL))
            if self.downloadModel(pathToFile):
                self.loadModel(pathToFile)
            else:
                raise SystemExit('%s cannot be loaded!' % self.chompTimeStamp(pathToFile))
    
    
    def chompTimeStamp(self, pathToFile):
        """
        Odsekne z cesty k modelu časové razítko
        
        Parameters
        -------
        pathToFile : string 
            Relatívna alebo absolútna cesta k súboru vrátane časového razítka oddeleného ":"
            
        Returns
        -------
        pathToFile : string 
            Relatívna alebo absolútna cesta k súboru bez razítka
        """
        try:
            path, timestamp = pathToFile.split(':')
            return path + ':'
        except ValueError:
            return pathToFile + ':'
        
    
        
    def downloadModel(self, pathToFile):
        model_Name = pathToFile.split('/')[-1]   # Nazov suboru
        model_path = pathToFile.split('/')[-2]   # Umiestnenie suboru
        try: 
            logging.info('DOWNLOADING MODEL FROM URL: %s' % (settings.MTA_MODEL_URL + model_path + '/' + model_Name))
            r = requests.get(url = settings.MTA_MODEL_URL + model_path + '/' + model_Name,
                         auth = (settings.MTA_MODEL_LOGIN, settings.MTA_MODEL_PASSWD),
                         stream = True)
            if r.status_code == 200:
                if not os.path.exists('/'.join(pathToFile.split('/')[:-1])):              # Vytvor adresar pre ulozenie modelu
                    os.makedirs('/'.join(pathToFile.split('/')[:-1]))
                total_length = r.headers.get('content-length')
                with open(pathToFile, 'wb') as f:
                    if total_length is None: # no content length header
                        f.write(r.content)
                    else:
                        dl = 0
                        total_length = int(total_length)
                        for data in r.iter_content(chunk_size=4096):
                            dl += len(data)
                            f.write(data)
                            done = int(50 * dl / total_length)
                            sys.stdout.write("\r[%s%s] \t %s %s/%s" % ('=' * done, ' ' * (50-done), model_Name, dl, total_length) ) 
                            sys.stdout.flush()
                        sys.stdout.write('\n%s succesfully downloaded \n' % model_Name)
                    f.close()
                    return True
            else: 
                return False
        except Exception:
            logging.error('Downloading model %s failed' % pathToFile.split('/')[-1])
            return False
             
        

    