# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 11:00:14 2019

@author: roman
"""
from .preprocessor import Preprocessor
from .vectorSpaceModel import VectorSpaceModel
from .clusterizer import Clusterizer
from .POS_Tagger import POS_Tagger
from .lemmatizer import Lemmatizer
from .aspectExtractor import AspectExtractor
import settings
import os

absolutePath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Trieda zostavi instanciu systemu so vsetkymi nastrojmi (preprocessor, vsm, clusterizer...)
# Nebude tak potrebne toto furt dookla rucne zostavovat v kazdom jednom skripte...
# Zatial skor pokus
class SystemFactory:
    """
    Inštancia systému zahrňujúca všetky modely a komponenty v jednom objekte
    
    Attributes
    -------
    preprocessor : Preprocessor
        Modul vykonávajúci predspracovanie textu
    vsm : VectorSpaceModel
        Model vektorového priestoru slov
    clusterizer : Clusterizer
        Model vykonávajúci zhlukovanie dokumentov na základe sémantickej podobnosti
    sentimentAnalyzer : SentimentAnalyzer
        Klasifikátor sentimentu dokumentov
    categoryClassifier: CategoryClassifier
        Klasifikátor kategorie dokumentov
    """
    
    
    def __init__(self, lemmatizerFirstOnly=False):
        self.lemmatizer = Lemmatizer(lemmatizerFirstOnly)
        self.preprocessor = Preprocessor(lemmatizer=self.lemmatizer)
        self.vsm = VectorSpaceModel()
        self.clusterizer = Clusterizer(vsm=self.vsm)
        # self.sentimentAnalyzer = SentimentAnalyzer(vsm=self.vsm)
        # self.posTagger = POS_Tagger()
        self.aspectExtractor = AspectExtractor(preprocessor=self.preprocessor, vsm=self.vsm)

    
    def build_load_system(self):
        """ 
        Zostaví a načíta jednotlivé modely definované v module settings alebo podla zadanych parametrov
        
        Returns
        -------
        SystemFactory objekt obsahujúci inicializovaný model VSM, Clusterizer, SentimentAnalyzer, TFIDF Vectorizer
        """
        # Vector Space Model
        # self.vsm.loadModel(pathToFile=settings.PATH_TO_VSM_MODEL)
        # Sentiment classifier
        # self.sentimentAnalyzer.loadModel(pathToFile=settings.PATH_TO_SENTIMENT_MODEL)
        # POS Tagger
        # self.posTagger.loadModel(settings.PATH_TO_POS_MODEL)
        return self
