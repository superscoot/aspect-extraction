# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 13:57:41 2019

@author: roman
"""
from .model import Model
import logging
import pandas as pd
import random
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.linear_model import SGDClassifier
import dill
import time
import datetime


class CategoryClassifier(Model):
    """
    Model klasifikátoru sa pokúsi zaradiť zadaný dokument do kategórie podľa jeho sémantického významu, podobne ako pri clusteringu.
    
    Parameters
    -------
    score : int
        Dosiahnutá úspešnosť modelu
    
    """
    
    def __init__(self, vsm=None):
        super().__init__(vsm=vsm)
        self.score = None
        
    
    def initModel(self, dataset, test_size=0.3):
        """
        Zo vstupných dát sa pokúsi natrénovať model klasifikátoru
        
        Parameters
        -------
        dataset : list of tuples 
            Prvou položkou je tokenizovaná veta, druhou je príznak kategorie (tag/label)
        test_size : float
            Pomerova velkost testovacej mnoziny vyclenenej zo vstupnych dat

        Returns
        -------
        self: SGDClassifier
        
        Examples
        -------
        >>> X = [(['dobry', 'objektiv'], 'celkový dojem'), (['slaba', 'vydrz', 'baterie'], 'výdrž baterie')]
        >>> cc = CategoryClassifier(tfidfVec)
        >>> cc.initModel(X, test_size=0.3)
        """ 
        # Zo zadaneho listu tuplov vytvori dateframe, vhodny pre trenovanie klasifikatoru
        df = pd.DataFrame(dataset, columns=['post', 'tag'])
        # Rozdel vstup na trenovaciu a testovaciu mnozinu v pomere podla zadaneho parametru
        train, test = train_test_split(df, test_size=test_size, random_state = random.randint(1,100))
        # Transformuj trenovaciu a test mnozinu do matice vektorov
        X_train = self.transform_to_vectors(train['post'])
        X_test = self.transform_to_vectors(test['post'])
        self.model = SGDClassifier(loss='hinge', penalty='l2',alpha=1e-3, random_state=42, max_iter=5, tol=None)
        self.model.fit(X=X_train, y=train['tag'])
        # Vyhodnot uspesnost modelu
        self.evaluate_model(X_test=X_test, y_test=test)
        return self.model
        
    
    def evaluate_model(self, X_test, y_test):
        """
        Vyhodnotí úspešnosť modelu
        
        Parameters
        -------
        X : List of NumPy arrays
            Vektor testovacích dát
        y_test : DataFrame
            Vzor testovacích dát
        """
        y_pred = self.model.predict(X_test)
        self.score = accuracy_score(y_pred, y_test['tag'])
        print('accuracy %s' % self.score )
        print(classification_report(y_test['tag'], y_pred))
      
        
    def predict(self, document):
        """
        Klasifikuje sentiment zadaného dokumentu.
        
        Parameters
        -------
        document : list reťazcov 
            Tokenizovaný, predspracovaný dokument
            
        Returns
        -------
            Príznak kategorie dokumentu
        
        Examples
        -------
        >>> cc = CategoryClassifier(w2vDict)
        >>> cc.predict(['dobry', 'objektiv'])
        """
        docVec = self.transform_to_vectors([document])
        return self.model.predict(docVec)
#        return self.model.predict_proba
    
    
    def transform_to_vectors(self, dataset):
        """
        Metóda prevedie dataset dokumentov do matice vektorov priestoru.
        
        Parameters
        -------
        dataset : DataFrame 
            Stĺpec post obsahuje tokenizovaný dokument. Stĺpec tag jeho sentiment.
        
        Returns
        -------
            NumPy array
        
        Examples
        -------
        >>> df = pd.DataFrame({'post': [['dobry', 'objektiv']], 'tag': [['positive']]  })
        >>> sa = SentimentAnalyzer(w2vDict)
        >>> sa.transform_to_vectors(dataset=df)
        """
        docVecs = self.vsm.transform(dataset)
        return docVecs
    
    
    def saveModel(self, pathToFile):
        """
        Uloží naučený model pre neskoršie použitie bez nutnosti opakovaného učenia
        
        Parameters
        -------
        pathToFile : string 
            Relatívna alebo absolútna cesta k uloženiu súboru
        """
        try:
            pathToFile = self.chompTimeStamp(pathToFile)
            timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M')
            f = open(pathToFile + timestamp, 'wb')
            obj = {
                'model': self.model,
                'score': self.score
                }
            dill.dump(obj, f)
            f.close()
            logging.info('model saved to %s' % pathToFile)
        except FileNotFoundError:
            logging.error('model failed to save: File %s not found!' % pathToFile)
