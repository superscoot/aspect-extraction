# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 10:20:09 2019

@author: roman
"""

class Product:
    
    
    def __init__(self, id, name, product_categories_id, retrieved_at = None):
        self.id = id
        self.name = name
        self.products_categories_id = product_categories_id
        self.retrieved_at = retrieved_at
        self.features = []

                
    def add_feature(self, feature):
        self.features.append(feature)
   
    
    # Metoda serializuje atributy pre citatelny vypis (napr. do JSONu)
    def serialize(self):
        features_serialized = [{
                'label': str(f.cluster_label),
                'text': f.raw_text,
                'sentiment': f.sentiment,
                'published_at': f.published_at.strftime("%Y-%m-%d"),
                'retrieved_at': f.retrieved_at.strftime("%Y-%m-%d")
            } for f in self.features
        ]
        
        return {
            'products_id': self.id,
            'name': self.name, 
            'product_categories_id': self.products_categories_id,
            'features': features_serialized,
#            'features': self.features
        }