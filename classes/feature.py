# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 15:43:41 2019

@author: roman
"""

class Feature: 
    
    def __init__(self, products_id, features_text_id, languages_id, text=None):
        self.products_id = products_id
        self.features_text_id = features_text_id
        self.text = text
        self.languages_id = languages_id