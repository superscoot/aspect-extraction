# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 09:34:08 2019

@author: roman
"""


from .model import Model
import majka
import settings
import os 
import logging


class Lemmatizer(Model): 
    
    
    def __init__(self, lemmatizerFirstOnly):
        self.model = self.loadModel(settings.PATH_TO_MAJKA_DICT)
        self.model.flags |= majka.ADD_DIACRITICS # dopln chybajucu diakritirku
        self.model.flags |= majka.IGNORE_CASE
        self.model.first_only = lemmatizerFirstOnly # vrat len prvy najdeny vyskyt slova
        self.model.tags = False # vrat len lemmu, znacky nespracovavaj 
        self.model.negative = 'ne' # Negacie transformuj do lemmy aj s predponou (nema => nemit)

    def getLemma(self, word):
        return self.model.find(word)
    
    
    def saveModel(self):
        # Abstraktna metoda, v tomto modeli nema zmysel ale musi tu byt kvoli dedicnosti
        pass
    
    
    def loadModel(self, pathToFile):  
        if not os.path.isfile(pathToFile):
            # Model neexistuje stiahni zo serveru
            model =  self.downloadModel(pathToFile)
            if not model:
                logging.error('Downloading model %s failed' % pathToFile.split('/')[-1])
        return majka.Majka(pathToFile)
        
            
                
    
