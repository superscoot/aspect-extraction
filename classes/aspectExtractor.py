# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 13:57:41 2019

@author: roman
"""
from .model import Model
import json
import numpy as np
from nltk import ngrams
from scipy import spatial
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import pandas as pd
import settings
import os

class AspectExtractor(Model):
    """
    Model klasifikátoru sa pokúsi identifikovať a klasifikovať aspekty zadaného dokumentu do kategórie podľa jeho sémantického významu, podobne ako pri clusteringu.
    """
    
    def __init__(self,preprocessor, vsm=None):
        super().__init__(vsm=vsm)
        self.preprocessor = preprocessor
        self.aspects = []
        self.aspect_docs = []
        self.aspect2label = {}
        self.aspect_vectors = None
        
        
    def load_aspects_candidates(self, domain_id):
        self.aspects = []
        self.aspect_docs = []
        self.aspect2label = {}
        self.aspect_vectors = None
        idx = 0
        pathToFile = settings.PATH_TO_ASPECT_CANDIDATE.format(domain_id)
        if not os.path.isfile(pathToFile):
            # Model neexistuje stiahni zo serveru
            self.downloadModel(pathToFile)
        for item in json.load(open(pathToFile)):
            docs = self.preprocessor.clean(list(set(item['documents'])))['cleaned']
            self.aspects.append({
                'label': item['label'],
                'docs_count': len(docs),
                'documents': docs
            })
            self.aspect_docs.extend(docs)
            for doc in docs:
                self.aspect2label[idx] = {'label': item['label'], 'cluster_id': item['cluster_id']}
                idx += 1
        self.aspect_vectors = self.vsm.transform(self.aspect_docs)
        
        
    def get_similarity_matrix(self, document_vector):
        matrix = []
        for asp_vec in self.aspect_vectors:
            matrix.append(spatial.distance.cosine(document_vector, asp_vec))
        return np.array(matrix)


    def get_most_similar_aspect_index(self, document):
        document_vector = self.vsm.transform([document])
        similarity_matrix = self.get_similarity_matrix(document_vector=document_vector)
        return np.argmin(similarity_matrix), np.min(similarity_matrix)
    

    def get_ngrams(self, document, N=5):
        # review ngrams
        ngrams_list = []
        for n in range(1,N):
            gram = ngrams(document, n)
            if gram:
                ngrams_list.extend(list(gram))
        return ngrams_list


    def calculate_score(self, ngram, similarity):
        #return 1 - ( (1/len(ngram) - simi) / 1)
        return (len(ngram) + (1-similarity)) / 4
        
    
    def find_aspect_and_label_by_numpy(self, document, similarity_threshold=0.1):
        obj = {}
        # Generuj ngramy
        ngrams_list = self.get_ngrams(document, N=5)
        # Vektorova reprezentacia ngramov
        ngrams_matrix = self.vsm.transform(ngrams_list)
        # Cosine similarity matrix (pocet ngramov x pocet aspektov) - riadky ngramy, stlpce aspekty. Hodnoty su cosinova podobnost
        ngram_aspect_similarity_matrix = 1 - np.dot(ngrams_matrix, self.aspect_vectors.T) / np.outer(np.linalg.norm(ngrams_matrix, axis=1), np.linalg.norm(self.aspect_vectors, axis=1))
        # Pre kazdy riadok (ngram) najdi najpodobnejsi aspekt
        for ngram_idx, row in enumerate(ngram_aspect_similarity_matrix):
            try: 
                idx = np.nanargmin(row)
                similarity = float(np.nanmin(row))
                if similarity <= similarity_threshold:
                    if self.aspect2label[idx]['label'] not in obj:
                        obj.update({self.aspect2label[idx]['label']: []})
                    score = self.calculate_score(ngram=ngrams_list[ngram_idx], similarity=similarity)
                    obj[self.aspect2label[idx]['label']].append({
                            'label': self.aspect2label[idx]['label'],
                            'cluster_id': self.aspect2label[idx]['cluster_id'],
                            'ngram': ngrams_list[ngram_idx],
                            'similar_to': ' '.join(self.aspect_docs[idx]), 
                            'cos_similarity': similarity, 
                            'our_score': score
                        })
            except ValueError:
                # Vynimka nastava v pripade, ze ngram_aspect_similarity_matrix obsahuje iba hodnoty NaN - typicky problem s out-of-dict words (preklepy, skomoleniny)
                continue
        # Vyber aspect s najvyssim skore
        for key, value in obj.items():
            value.sort(key=lambda x: x['our_score'], reverse=True) 
        aspects = self.unify_aspects({key: value[0] for key, value in obj.items()})
        return {'text_nlp': document, 'aspects': aspects}
    
    
    # Vykresli VSM aspekty do grafu (zhluky)
    def plot_clusters(self):
        tsne_init = 'pca'
        tsne_perplexity = 20.0
        tsne_early_exaggeration = 4.0
        tsne_learning_rate = 10
        model = TSNE(n_components=2, random_state=0, init=tsne_init, perplexity=tsne_perplexity,
                 early_exaggeration=tsne_early_exaggeration, learning_rate=tsne_learning_rate)     
        reducedDimensions = model.fit_transform(self.aspect_vectors)
        labels = pd.factorize(pd.DataFrame(self.aspect2label.values())[0])[0]
        labels = labels.reshape(self.aspect_vectors.shape[0], 1)
        reducedDimensions = np.append(reducedDimensions, labels, axis=1)
        plt.scatter(reducedDimensions[:,0], reducedDimensions[:,1], c=reducedDimensions[:,2])
    
    
    def saveModel():
        pass
        
    
    def intersection(self, lst1, lst2): 
        # Prienik zadanych listov
        lst3 = [value for value in lst1 if value in lst2] 
        return lst3 
        
    
    def unify_aspects(self, aspects):
        # Ak 2 rozne aspekty obsahuju spolocne slovo metoda vyberie ten aspekt, ktory ziskal vyssie skore
        if not aspects:
            return aspects
        ngrams_list = [[aspects[key]['ngram'], key] for key in aspects.keys()]
        filtered_aspects = {}
        intersections = []
        for idx, ngram in enumerate([ng[0] for ng in ngrams_list]):
            for idx1, ngram1 in enumerate([ng[0] for ng in ngrams_list]):
                if ngram != ngram1:
                    inter = self.intersection(ngram, ngram1)
                    if inter:
                        intersections.append((ngrams_list[idx][1], ngrams_list[idx1][1]))
        for i in intersections:
            higher = i[0] if aspects[i[0]]['our_score'] >  aspects[i[1]]['our_score'] else i[1]
            filtered_aspects.update({higher: aspects[higher]})
        intersect_keys = []
        for k in intersections:
            intersect_keys.extend(k)
        for key in aspects.keys():
            if key not in intersect_keys:
                filtered_aspects.update({key: aspects[key]})
        return filtered_aspects
    
    
    




