# -*- coding: utf-8 -*-

### Trieda riesiaca preprocessing dat - odstranenie neziaducich znakov, stop slov atd.

import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import re
from nltk.tokenize import sent_tokenize
from nltk import RegexpTokenizer
from nltk import word_tokenize
from nltk import download
from unidecode import unidecode
import sys
import settings
from classes.POS_Tagger import POS_Tagger
import dill
import logging
from tqdm import tqdm

download('punkt')
'''
---Preprocessing
 - tokenizuj podla regexp => podla medzier to nie je uplne ono
 - lowercase 
 - pos_tag
 - stopwords => odstran castice, spojky, neplnovyznamove
 - odstran prilis dlhe slova (nad 30 znakov?)
 - frekvenciu termov neriesme
'''


class Preprocessor:
    
    
    def __init__(self, lemmatizer=None):
       self.lemmatizer = lemmatizer
       self.pt = POS_Tagger()
        # self.pt.loadModel(pathToFile=settings.PATH_TO_POS_MODEL)
       self.rawDocs = None
       self.cleanedDocs = None
       self.params = {}
       self.stopwords = [unidecode(word) for word in self.lemmatize([item for item in settings.FORBIDDEN_WORDS])]

   
       
    # Metoda tokenizuje a predspracuje zadany vstup. Pre kazdy dokument je volana metoda simplify
    # clean vrati tuple spracovanych dokumentov aj ich povodonych tvarov
    # Input: List retazcov: ['Foo foo.', 'Bar, bar bar!']
    def clean(self, dataset, verbose=False):
#        logging.info('Preprocessing documents')
        cleanedDocs = []
        rawDocs = []
        # Tokenizacia datasetu podla zadanych parametrov
        if settings.TOKENIZE_SENTENCES:
            dataset = self.tokenize_sentences(input_docs=dataset)
        documentCount = sum(1 for line in dataset)
        for idx, document in enumerate(dataset):
            document = self.simplify(document)
            if document[0]:
                cleanedDocs.append(document[0])
                rawDocs.append(document[1].strip().lower())
        self.cleanedDocs = cleanedDocs
        self.rawDocs = rawDocs
        return {'cleaned': self.cleanedDocs, 'raw': self.rawDocs, 'params': self.params}
              
    
    # Tokenizuje zadany text podla viet
    # Input: ['Foo foo. Bar bar bar.']
    # Output: ['Foo foo', 'Bar bar bar']
    def tokenize_sentences(self, input_docs):
        dataset = []
        [dataset.extend(sent_tokenize(doc)) for doc in input_docs] # Tokenizuj vety
        self.params['tokenize_sentences'] = 1
        return dataset
    

    # Tokenizuje zadany text podla carok
    # Input: 'Bezkonkurenční kvalita,Vyklápěcí displej, Skvělé fotky na vysoké ISO'
    # Output: ['Bezkonkurenční kvalita', 'Vyklápěcí displej', 'Skvělé fotky na vysoké ISO']
    def tokenize_by_comma(self, input_docs):
        tokenized = []
        for sentence in input_docs:
            sentence = re.sub('(\d+),(\d+)', '\g<1>.\g<2>', sentence)
            tokenized.extend(sentence.split(','))
        return tokenized                


    # Slovny tokenizer
    def word_tokenizer(self, document, language): 
        self.params['word_tokenizer'] = language
        return word_tokenize(document, language=language)


    # Nahradi vsetky desatinne ciarky desatinnymi teckami           
    def replace_comma_in_digits(self, input_doc):
        self.params['replace_comma_in_digits'] = 1
        return re.sub('(\d+),(\d+)', '\g<1>.\g<2>', input_doc)
    

    # Vrati tokenizovany dokument ocisteny od stopslov a balastu
    def simplify(self, document):
        raw_document = document
        # Lowercase
        document = document.lower()
        self.params['lowercase'] = 1
        # Odstran url a http odkazy
        document = self.removeURL(document)
        # Tokenizuj dokument
        document = self.word_tokenizer(document, language='czech')
        # Lemmatizuj kazde slovo dokumentu
#        document = self.lemmatize(document)
#        self.params['lemmatize'] = 1        
        # Odstran nealfanumericke znaky
        document = self.removeNotAlphaNum(document)
        # Redukcia stop slov
        if settings.REMOVE_STOPWORDS:
            document = self.removeStopwords(document)
        # Odstran diakritiku
        document = self.removeAccents(document)
        # Redukcia extremne dlhych slov
        document = self.removeExtremeLongWords(document)
        return document, raw_document

    
    # Odstran url a odkazy z textu
    def removeURL(self, document):
        self.params['removeURL'] = 1
        return ' '.join([re.sub(r'(http.+)|www.+', '', word) for word in document.split(' ')])
    
    
    # Odstran diakritiku
    def removeAccents(self, document):
        self.params['removeAccents'] = 1
        if not document: 
            return None
        else:
            return [unidecode(word) for word in document]

    
    # Vypusti z textu slova, ktorych dlzka prekroci povolenu hranicu
    def removeExtremeLongWords(self, document):
        self.params['removeExtremeLongWords'] = 1
        if not document: 
            return None
        else:
            return [word for word in document if len(word) <= settings.MAX_TERM_LENGTH]


    # Odstani nelafanumericke znaky zo slov
    def removeNotAlphaNum(self, document):
        self.params['removeNotAlphaNum'] = 1
        return [re.sub(r'[\W]', '', word) for word in document]
            
    

    # Vypusti z dokoumentu slovne druhy, ktore su definovane ako stopslova
    # Viz settings.py
    # Rusime pos tagger pre filtraciu stop slov
    def removeStopwords(self, document):
        self.params['removeStopwords'] = 1
        cleaned_doc = []
        for word in document:
                if re.match('\w+', word) and (unidecode(self.lemmatize([word])[0]) not in self.stopwords):
                    cleaned_doc.append(word)
        return cleaned_doc

      
    # Vrati lemmu zadaneho slova. Zaroven opravi slovo bez zadanej diakritiky - velky --> velký
    # Ak lemma neexistuje, vrati povodny tvar slova.
    def lemmatize(self, document):
        lemmatized = []
        for word in document:
            morph = self.lemmatizer.getLemma(word=word)
            if morph:
                lemmatized.append(morph[0]['lemma'].lower())
            else:
                lemmatized.append(word)
        return lemmatized
        
        
    
    # Zapise spracovany vystup do binarneho suboru
    def saveOutput(self, pathToFile):
        f = open(pathToFile, 'wb')      
        obj = {'cleaned': self.cleanedDocs, 'raw': self.rawDocs}
        dill.dump(obj, f)
        f.close()