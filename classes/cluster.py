"""
Created on Wed Jul  4 14:00:14 2018

@author: roman
"""
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

class Cluster:
    
    def __init__(self, label, centroid, documents=None, title=None):
        self.label = label
        self.centroid = centroid
        self.documents = documents
        self.title = title
            
        
    def dump(self):
        centre_dist = [doc.get_centroid_dist() for doc in self.documents] 
        zippedDocuments = list(zip([doc.get_text() for doc in self.documents], centre_dist))
        countDocuments = {doc.get_text(): {'raw_text': doc.get_raw_text(), 'count': 0, 'distance': 0} for doc in self.documents} # inicializuj counter jednotlivych recenzii (kvoli duplicite)
        for doc in zippedDocuments:
            countDocuments[doc[0]]['count'] += 1
            countDocuments[doc[0]]['distance'] = doc[1] 
        sortedDocuments = sorted([(v['raw_text'], k, v['distance'], v['count']) for k, v in countDocuments.items()], key=lambda x: x[3]/(x[2]+1), reverse=True)
        return {
                'label': str(self.label),                # identifikator clustru, cislo, ziaden semanticky vyznam
                'title': self.title,                     # Slovny nazov clustru
                'meanDistance': self.get_mean_dist(),    # priemer vzdialenosti od centra clustru
                'stdDeviation': self.get_st_deviation(), # smerodatna odchylka od centra clustru
                'clusterSize' : sum([doc[3] for doc in sortedDocuments]),    # pocet dokumentov spadajucich do clustru 
                'ngramsFrequencies': {item[0]: int(item[1]) for item in self.get_ngram_freq()},
                'documents': sortedDocuments,            # dokument a jeho vzdialenost od centroidus
                }
            
        
    # Vrati priemer vzdialenosti dokumentov od centroidu
    def get_mean_dist(self):
        return np.mean([doc.get_centroid_dist() for doc in self.documents])
    
    
    # Vrati smerodatnu odochylku vzdialenosti dokumentov od centroidu
    def get_st_deviation(self):
        return np.std([doc.get_centroid_dist() for doc in self.documents])

    
    # Vrati rozptyl odochylku vzdialenosti dokumentov od centroidu
    def get_variance_dist(self):
        return np.var([doc.get_centroid_dist() for doc in self.documents])


    # Vrati list dokumentov, ktore patria do clustru
    def get_documents(self):
        return self.documents
    
    
    # Vrati suradnice centroidu
    def get_centroid(self):
        return self.centroid
    

    # Vytvori dict ngramov a spocita ich frekvenciu v ramci clustru - mozno pouzit ako inform summary    
    def get_ngram_freq(self, ngram_range=(1, 3)):       
        if not self.documents:
            return []
        else:
            word_vectorizer = CountVectorizer(ngram_range=ngram_range, analyzer='word')
            sparse_matrix = word_vectorizer.fit_transform([doc.get_text() for doc in self.documents])
            frequencies = sum(sparse_matrix).toarray()[0]
            words = word_vectorizer.get_feature_names()
            return sorted(zip(words, frequencies), key=lambda x: x[1], reverse=True)[:4]
        

        
        
    


