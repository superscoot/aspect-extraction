"""
Trieda inicializuje a natrenuje model na zadanej mnozine dokumentov. 
Vstupne dokumenty sa odovzdavaju ako pole. Je vhodne, aby boli dokumenty predspracovane, 
tzn. boli z nich odstranene stopslova, lemmatizovane termy, interpunkcia atd.


Created on Tue Jun  5 09:23:09 2018

@author: roman
"""

from .model import Model
import gensim
import logging
import numpy as np
import time
import datetime
import dill
import os

class VectorSpaceModel(Model):
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)    
    
    def __init__(self):
        super().__init__()
    
    # Iniciuje ucenie prazdneho modelu
    def initModel(self, trainingSet, vector_size=120):       
        self.model = gensim.models.Word2Vec(trainingSet, size=vector_size, window=5, workers=8) 
     
    
    # Dotrenuj existujuci model
    def trainModel(self, trainingSet):
        self.model.build_vocab(trainingSet, update=True)
        self.model.train(trainingSet, total_examples=self.model.corpus_count, epochs=self.model.epochs)
        self.last_update = datetime.datetime.now()
        
    
    
    # Vrati pole najpodobnejsich vyrazov k zadanemu termu
    def getMostSimilar(self, term):
        return self.model.wv.most_similar(positive=term)
    
    
    # Vrati mieru podobnosti medzi zadanymi termami
    def getSimilarity(self, term1, term2):
        return self.model.wv.similarity(term1, term2)
    
    
    # Vrati slovnik termov a ich suradnic v priestore
    def getDictionary(self):
        return dict(zip(self.model.wv.index2word, self.model.wv.vectors))
    
    
    def transform(self, document):
        """
        Vrati vektor dokumentu, tzn. priemer vektorov jednotlivych slov. 
        Ak sa nejake slovo nenachadza v slovniku, je mu priradeny nulovy vektor.
        
        Parameters
        -------
        document : list of strings 

        Returns
        -------
        self: np.array
        
        Examples
        -------
        >>> document = ['dobry', 'vykon', 'baterie']
        >>> vsm.transformSentence(document)
        """ 
        word2vecDict = self.getDictionary()
        return np.array([
                np.mean([word2vecDict[w] for w in words if w in word2vecDict] or 
                        [np.zeros(self.model.vector_size)], axis=0)
                for words in document
            ])   

    
   # Ulozi nauceny model
    def saveModel(self, pathToFile, archive=True):
        if archive:
            self.archiveModel(pathToFile)
        f = open(pathToFile, 'wb')       
        obj = {
            'model': self.model,
            'last_update': self.last_update,
        }
        dill.dump(obj, f)
        f.close()
        
        
        
#    def loadModel(self, pathToFile):
#        try:
#            self.model = gensim.models.Word2Vec.load(pathToFile)
#        except FileNotFoundError:
#            if self.downloadModel(pathToFile):
#                self.model = gensim.models.Word2Vec.load(pathToFile)
            
    