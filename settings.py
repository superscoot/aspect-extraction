# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 10:27:08 2018
@author: roman

Vyznam morfologicky znacek 
N 	substantivum (podstatné jméno)
A 	adjektivum (přídavné jméno)
P 	pronomen (zájmeno)
C 	numerál (číslovka, nebo číselný výraz s číslicemi)
V 	verbum (sloveso)
D 	adverbium (příslovce)
R 	prepozice (předložka)
J 	konjunkce (spojka)
T 	partikule (částice)
I 	interjekce (citoslovce)
X 	neznámý, neurčený, neurčitelný slovní druh
Z 	interpunkce, hranice věty 
"""

from unidecode import unidecode
import os

# Pripojenie k databazam

''' DB POSTGRES CONFIG '''
DB_HOST = '172.17.0.1'
DB_USERNAME = 'postgres'
DB_PASSWD = 'mta2018'
DB_NAME = 'product_reviews_devel'
DB_PORT = '15432'


''' MONGO DB CONFIG '''
# POZOR, je potrebne vytvorit tunel na mta.pef.mendelu.cz!
MONGO_HOST = 'GITLAB_DB_HOST'
MONGO_USERNAME = 'GITLAB_MONGO_USERNAME'
MONGO_PASSWD = 'GITLAB_MONGO_PASSWD'
MONGO_PORT = 'GITLAB_MONGO_PORT'


# Absolutna cesta k pracovnej zlozke
absolute_path = os.path.dirname(os.path.abspath(__file__))

''' MTA MODEL URL '''
MTA_MODEL_URL = 'http://mta.pef.mendelu.cz/~xvalovic/models/'
MTA_MODEL_LOGIN = 'mta'
MTA_MODEL_PASSWD = 'mta2018'

# PRIVATE ENDPOINT TOKEN
API_KEY = 'GITLAB_API_KEY'


''' PREPROCESSING '''
# Maximalna dlzka slova ktore prijmeme (inak sa zrejme jedna o preklep)
MAX_TERM_LENGTH = 30
# Parameter tokenizacie na vety
TOKENIZE_SENTENCES = True
# Parameter tokenizacie podla oddelovaca
TOKENIZE_BY_COMMA = False
# Odstranuj z dokumentov stopslova
REMOVE_STOPWORDS = True


''' LEMMATIZER '''
# Cesta ku slovniku lemmatizeru majka
PATH_TO_MAJKA_DICT = absolute_path + '/models/Lemmatizer/majka.w-lt'


''' POS Tagger '''
# Cesta k ulozenemu modelu pre znackovanie slovnych druhov
PATH_TO_POS_MODEL = absolute_path + '/models/POS/100k_pos_model.plk'
# Povolene plnovyznamove slovne druhy, ktore zachovame
ALLOWED_TAGS = ('N', 'A', 'V', 'D')


''' STOP WORDS'''
# Zakazane slova (pre ktore sa napr. nespravne urci slovny druh)
PATH_TO_STOPWORDS = absolute_path + '/dict/stopwords_czech_general'
FORBIDDEN_WORDS = [] # stop slova zbavene diakritiky
FORBIDDEN_WORDS_ACCENTS = [] # stop slova s diakritikou
with open(PATH_TO_STOPWORDS, 'r') as f:
    for line in f.readlines():
        FORBIDDEN_WORDS.append(unidecode(line.strip().lower()))
        FORBIDDEN_WORDS_ACCENTS.append(line.strip().lower())
f.close()
FORBIDDEN_WORDS = set(FORBIDDEN_WORDS)
FORBIDDEN_WORDS_ACCENTS = set(FORBIDDEN_WORDS_ACCENTS)


''' VSM '''
# Cesta k ulozenemu Vector Space Model
# PATH_TO_VSM_MODEL = absolute_path + '/models/VSM/digimanie_reviews_mongo_refer_raw'
PATH_TO_VSM_MODEL = absolute_path + '/models/VSM/'


''' CLUSTERING '''
PATH_TO_CLUSTER_MODEL = absolute_path + '/models/Clusterizer/mobiles:2019-07-30-14-02'


''' SENTIMENT ANALYSIS '''
PATH_TO_SENTIMENT_MODEL = absolute_path + '/models/Sentiment/sentiment_classifier'


''' CATEGORY CLASSIFIER '''
PATH_TO_CATEGORY_CLASSIFIER = absolute_path + '/models/Category_classifier/category_cellphones'


''' PHRASES IDENTIFIER '''
PATH_TO_PHRASES_IDENTIFIER = absolute_path + '/models/Phrases/phrases' 


''' ASPECT CANDIDATES ''' 
PATH_TO_ASPECT_CANDIDATE = absolute_path + '/models/Aspect_phrases/{}.json'