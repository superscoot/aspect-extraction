# -*- coding: utf-8 -*-

"""
Created on Tue Nov  6 14:36:11 2018

@author: roman
"""
import os, sys
#sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from classes.systemFactory import SystemFactory
import dill
from database.mongo import Mongo
import settings
import time
import logging
import datetime


t0 = time.time()
dataset = []

# Factory builder
system = SystemFactory()
system.build_load_system()


mongo = Mongo()
mongo.connect(database='mta_products', collection='refer')

with open('./train/All_texts.txt', 'r') as f:
        dataset = f.readlines()
    


for item in mongo.get_all_text_by_category(categories=['lenses', 'cameras', 'cellphones', 'televisions', 'smartwatches', 'notebooks'], from_date=datetime.datetime(1900, 1, 1)):
    if 'article_text' in  item:
        dataset.extend(item['article_text'])    
    for sentiment in ('pros', 'cons'):
        if 'author_sentiment' in item and sentiment in item['author_sentiment']:
            dataset.extend(item['author_sentiment'][sentiment])


dataset_nlp = system.preprocessor.clean(dataset, verbose=True)
### Vystup preprocessingu sa moze ulozit do suboru
system.preprocessor.saveOutput('digimanie_mongo_corpus_processed_raw.bin')
#dataset = dill.load(open('digimanie_mongo_corpus_processed.bin', 'rb'))


## create list of lists of unigrams

   
#samples = [system.phrasesIdentifier.analyzePhrases(sentence) for sentence in dataset['cleaned']]


### Inicializuj novy model alebo nacitaj existujuci
system.vsm.initModel(trainingSet=dataset_nlp['cleaned'], vector_size=300)
#    system.vsm.trainModel(samples)
system.vsm.saveModel(settings.PATH_TO_VSM_MODEL, archive=False)
logging.info('Hotovo za %s' % int(time.time()-t0))



system.vsm.model['design']
    

# chyba item 21276

model = system.vsm.model

model.predict_output_word(['dlouhá', 'výdrž'])

model.predict_output_word('obrovské množství'.split())



    
    