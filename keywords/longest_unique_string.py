import re
from nltk import word_tokenize


def longest_unique_string(most_frequent_word, system):
    longest_unique_string_list = most_frequent_word.copy()
    for m in reversed(most_frequent_word):
        for n in most_frequent_word[:most_frequent_word.index(m)]:
            if m != n:
                m_list = word_tokenize(rm_diacritics(m))
                n_list = word_tokenize(rm_diacritics(n))
                m_list = rm_diacritics_from_list(m_list)
                n_list = rm_diacritics_from_list(n_list)
                if system:
                    m_list = lemmatize_list(system, m_list)
                    n_list = lemmatize_list(system, n_list)
                intersection = set(m_list) & set(n_list)

                if len(intersection) == len(m_list) and len(intersection) < len(n_list):
                    rm_item(m, longest_unique_string_list)
                elif len(intersection) == len(n_list) and len(intersection) < len(m_list):
                    rm_item(n, longest_unique_string_list)
                elif len(intersection) == len(n_list) and len(intersection) == len(m_list):
                    index_n = most_frequent_word.index(n)
                    index_m = most_frequent_word.index(m)
                    rm_index = max([index_m, index_n])
                    rm_item(most_frequent_word[rm_index], longest_unique_string_list)
    return longest_unique_string_list


def rm_item(item, modified_dict):
    if item in modified_dict:
        modified_dict.remove(item)


def rm_diacritics(text):
    diacritics = {
        'ě': 'e',
        'š': 's',
        'č': 'c',
        'ř': 'r',
        'ž': 'z',
        'ý': 'y',
        'á': 'a',
        'í': 'i',
        'é': 'e',
        'ú': 'u',
        'ů': 'u',
        'ť': 't',
        'ď': 'd',
        'ó': 'o',
        'ň': 'n',
        ',': '',
        '.': '',
        '!': ''
    }
    pattern = '|'.join(sorted(re.escape(k) for k in diacritics))
    text = re.sub(pattern, lambda m: diacritics.get(m.group(0)), text, flags=re.IGNORECASE)
    return text


def rm_diacritics_from_list(list_of_words):
    return [rm_diacritics(l) for l in list_of_words]


def lemmatize_list(system, list_unlemmatized_words):
    list_lemmatized_words = []
    for w in list_unlemmatized_words:
        lemma = system.lemmatizer.getLemma(w)
        if lemma == []:
            list_lemmatized_words.append(w)
        else:
            list_lemmatized_words.append(lemma[0]['lemma'])
    return list_lemmatized_words
