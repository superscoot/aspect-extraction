import json
# import settings
import os

from sklearn.feature_extraction.text import CountVectorizer

from .get_most_similar_documents import get_most_similar_documents
from .longest_unique_string import longest_unique_string

absolute_path = os.path.dirname(os.path.abspath(__file__))

# stop_words = settings.FORBIDDEN_WORDS
with open(absolute_path + '/useless_words.json', 'r') as f:
    useless_words = json.load(f)


def get_most_frequent(all_sentences=None, top=5, n_gram_start=1, n_gram_end=3,
                      n_grams=[], distance='jaro', category_name='', system=None):
    try:
        # all_sentences, mapping_dict, not_lemmatized = lemmatizate(all_sentences, nlp,                                                                   correct_diacritics)
        # Najdi nejfrekventovanejsi n-gramy
        vectorizer = CountVectorizer(analyzer='word',
                                     # stop_words=stop_words,
                                     ngram_range=(n_gram_start, n_gram_end),
                                     min_df=2,
                                     token_pattern='\d+ \w{1,4}|[\w\d]{2,}|\d+\.\d+ \w{1,4}|\d+\.\d+ \w{1,4}/\w{1,4}|\w+-\w+')

        n_grams = [n.replace(',', '') for n in n_grams]
        vec = vectorizer.fit(n_grams)
        X = vec.transform(n_grams)

        sum_words = X.sum(axis=0)
        words_freq = [[word, sum_words[0, idx]] for word, idx in vec.vocabulary_.items()]
        words_freq = sorted(words_freq, key=lambda x: (x[1]), reverse=True)
        words_freq = [word[0] for word in words_freq if word[0] not in useless_words]
        corrected_list = longest_unique_string(words_freq[:top * 4], system=None)

        if corrected_list:
            list_most_similar = get_most_similar_documents(all_sentences, corrected_list[:top],
                                                           distance=distance, category_name=category_name,
                                                           system=system)

            return list_most_similar
        return None
    except ValueError:
        return None
