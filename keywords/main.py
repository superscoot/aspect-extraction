# -*- coding: utf-8 -*-
from datetime import date

import psycopg2
import stanfordnlp
from sshtunnel import SSHTunnelForwarder
import mta_server_settings
from get_most_frequent import get_most_frequent
import time


def lemmatizate_feature_names(c_name):
    lemm_category_name = {}
    for v in c_name:
        if not v[2] in lemm_category_name.keys():
            lemm_category_name[v[2]] = {}
        if ' ' in v[1]:
            doc = nlp(v[1])
            name = []
            for w in doc.sentences[0].words:
                name.append(w.lemma)
            lemm_category_name[v[2]][v[0]] = name
        else:
            lemm_category_name[v[2]][v[0]] = [v[1]]
    return lemm_category_name


DB_HOST = 'localhost'
DB_USERNAME = 'postgres'
DB_PASSWD = 'mta2018'
DB_NAME = 'product_reviews_devel'
DB_PORT = '5432'

try:
    connection_string = "dbname=%s user=%s host=%s password=%s port=%s" % (DB_NAME,
                                                                           DB_USERNAME,
                                                                           DB_HOST,
                                                                           DB_PASSWD,
                                                                           DB_PORT)

    PORT = 5432
    tunnel = SSHTunnelForwarder(('mta.pef.mendelu.cz', 22),
                                ssh_username=mta_server_settings.USERNAME,
                                ssh_password=mta_server_settings.PASSWD,
                                remote_bind_address=('localhost', PORT),
                                local_bind_address=('localhost', PORT))

    tunnel.start()
    # SQL queries
    postgres_query_s_reviews = """SELECT text FROM reviews 
                                    WHERE products_id = %s AND feature_names_id = %s"""
    postgres_query_s_feature_names = """SELECT feature_names_id, text, product_categories_id FROM feature_names 
                                    WHERE product_categories_id IN (SELECT DISTINCT(product_categories_id)
                                    FROM products
                                    WHERE products_id IN (SELECT DISTINCT(products_id)
                                        FROM reviews  LEFT JOIN product_features_keywords USING (products_id)
                                        WHERE reviews.inserted_at > COALESCE (product_features_keywords.inserted_at,to_timestamp(0)))
                                    ) AND languages_id = %s
                                    ORDER BY product_categories_id"""
    postgres_query_i_keywords = """INSERT INTO product_features_keywords (products_id, feature_names_id,description, languages_id, inserted_at) VALUES (%(products_id)s,%(feature_name_id)s,%(description)s,%(languages_id)s, %(inserted_at)s)"""
    postgres_query_s_keywords = """SELECT description FROM product_features_keywords WHERE products_id = %(products_id)s AND feature_names_id = %(feature_name_id)s AND languages_id = %(languages_id)s"""
    postgres_query_u_keywords = """UPDATE product_features_keywords 
                                    SET description = %(description)s,
                                    inserted_at = %(inserted_at)s
                                    WHERE products_id = %(products_id)s AND feature_names_id = %(feature_name_id)s AND languages_id = %(languages_id)s"""
    postgres_query_s_feature_categories = """SELECT product_categories_id FROM product_categories"""
    postgres_query_s_products = """SELECT products_id, product_categories_id
                                    FROM products
                                    WHERE products_id IN (SELECT DISTINCT(products_id)
                                        FROM reviews  LEFT JOIN product_features_keywords USING (products_id)
                                        WHERE reviews.inserted_at > COALESCE (product_features_keywords.inserted_at,to_timestamp(0)))
                                    ORDER BY product_categories_id"""

    # establish conncetion do postgres
    connection = psycopg2.connect(connection_string)
    connection.autocommit = True
    cursor = connection.cursor()

    # settings
    top = 5
    n_gram_start = 1
    n_gram_end = 3
    nlp = stanfordnlp.Pipeline(processors='tokenize,pos,mwt,lemma', lang='cs', tokenze_pretokenized=True)
    languages_id = 1
    inserted_at = date.today()

    cursor.execute(postgres_query_s_feature_names, (languages_id,))
    feature_name_tuples = cursor.fetchall()
    lemm_category_name = lemmatizate_feature_names(feature_name_tuples)
    cursor.execute(postgres_query_s_products, )
    products_ids = cursor.fetchall()
    t0 = time.time()
    t1 = t0
    for products_id_tupple in products_ids:
        print(products_ids.index(products_id_tupple), "_of_", len(products_ids), end='\r')

        products_id = products_id_tupple[0]
        for t in lemm_category_name[products_id_tupple[1]].keys():

            # set query params
            feature_name_id = t
            # select reviews and generate keywords
            cursor.execute(postgres_query_s_reviews, (products_id, feature_name_id))
            reviews = [r[0] for r in cursor.fetchall()]
            if reviews:
                text = get_most_frequent(reviews, top, n_gram_start, n_gram_end, nlp,
                                         lemm_category_name[products_id_tupple[1]][feature_name_id],
                                         adjective_only=False, joined=False)
                # insert or update database
                if text:
                    parameters = {'products_id': products_id, 'feature_name_id': feature_name_id,
                                  'description': text, 'languages_id': languages_id, 'inserted_at': inserted_at}
                else:
                    parameters = {'products_id': products_id, 'feature_name_id': feature_name_id,
                                  'description': None, 'languages_id': languages_id, 'inserted_at': inserted_at}
                cursor.execute(postgres_query_s_keywords, parameters)
                existingDescription = cursor.fetchone()
                if existingDescription:
                    cursor.execute(postgres_query_u_keywords, parameters)
                else:
                    cursor.execute(postgres_query_i_keywords, parameters)
    t3 = t0 - time.time()
    print('t3', t3)
except (Exception, psycopg2.Error) as error:
    if (connection):
        print("Failed: ", error)
finally:
    # closing database connection.
    if (connection):
        cursor.close()
        connection.close()
        tunnel.stop()
        print("PostgreSQL connection is closed")
