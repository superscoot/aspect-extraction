import re

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel, paired_euclidean_distances, pairwise_distances

# from app.core.keywords.longest_unique_string import longest_unique_string
from keywords.longest_unique_string import longest_unique_string
import textdistance

MAXIMAL_LENGTH_SENTENCE = 5


# TODO  oprava vet, ktere obsahuji carky bez mezer

def filter_all_sentences(all_sentences):
    return [s for s in all_sentences if len(s.split(' ')) <= MAXIMAL_LENGTH_SENTENCE]


def get_most_similar_documents(all_sentences, candidates, distance, category_name, system):
    all_sentences = filter_all_sentences(all_sentences)
    if distance == 'euklidian' or distance == 'cosine':
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_vec = tfidf_vectorizer.fit_transform(all_sentences)
    else:
        tfidf_vectorizer = None
        tfidf_vec = None
    # typical_document_dict = {}
    list_most_similar = set()

    for c in candidates:
        most_similar, similarity = get_typical_document(c, all_sentences, distance, tfidf_vec, tfidf_vectorizer)
        if most_similar != None:
            list_most_similar.add(most_similar)
            # typical_document_dict[c] = (most_similar, similarity)
    list_most_similar = longest_unique_string(list(list_most_similar), system)

    list_most_similar = remove_useless_characters(list_most_similar, category_name)

    return list_most_similar


def get_typical_document(candidate, all_documents, distance='jaro', tfidf_vectors=None, tfidf_vectorizer=None):
    if distance == 'cosine':
        candidate_vector = tfidf_vectorizer.transform([candidate])
        cosine_similarities = linear_kernel(candidate_vector, tfidf_vectors).flatten()
        cosine_similarities = list(enumerate(cosine_similarities))
        most_similar, similarity = max(cosine_similarities, key=lambda t: t[1])
        most_similar = all_documents[most_similar]
    if distance == 'euklidian':
        candidate_vector = tfidf_vectorizer.transform([candidate])
        euclidean_similarities = pairwise_distances(candidate_vector, tfidf_vectors).flatten()
        euclidean_similarities = list(enumerate(euclidean_similarities))
        most_similar, similarity = min(euclidean_similarities, key=lambda t: t[1])
        most_similar = all_documents[most_similar]
    if distance == 'jaro':
        # jr = JaroWinkler()
        similarity_all_documents = []
        for d in all_documents:
            distance = textdistance.jaro.normalized_distance(candidate, d)
            similarity_all_documents.append([d, distance])
        most_similar, similarity = min(similarity_all_documents, key=lambda t: t[1])
    return most_similar, similarity


def remove_useless_characters(list_most_similar, category_name):
    # pro opravu  recenzi
    removed_char = ['\.$', ',$', ' $', '!!!$', '!!$', '!$', '\?$', ' \.\.\.$', '\.\.\.$', ':-\)', ':\)', '\*$', '\+\+$']
    corrected_list = []
    for l in list_most_similar:
        if l.lower().strip() not in category_name:
            pattern = '|'.join(removed_char)
            corrected_list.append(re.sub(pattern, '', l))
    return corrected_list
